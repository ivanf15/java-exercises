package com.examples.exercises;

class HelloMessage {
    private String message = "";

    public HelloMessage() {
        message = "Default Message";
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message.toUpperCase();
    }
}

public class Main {

    public static void main(String[] args) {
        if (args.length > 0) {
            for (String arg : args)
                System.out.println(arg);
        }
        
        HelloMessage hm = new HelloMessage();
        System.out.println(hm.getMessage());
        hm.setMessage("Hello world");
        System.out.println(hm.getMessage());
    }
}
