import java.sql.*;

public class Main {

    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1/ad_79c75a0c585818b?user=root&password=Mo14Iv15&serverTimezone=UTC");

        System.out.println("Connected");

        final String query = "select * from ad_79c75a0c585818b.post_types";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while(resultSet.next()){
            int id= resultSet.getInt("id");
            String name = resultSet.getString("name");

            System.out.println("id = " + id + " name = " + name);
        }

        resultSet.close();
        connection.close();
    }
}
