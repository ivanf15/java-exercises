package com.java8;

class A {
    A get(){
        return this;
    }
}
class B extends A{

    @Override
    B get() {
        return this;
    }

    void me (){
        System.out.println("askhgd");
    }
}

public class Overriding {

    public static void main(String[] args) {
        B asd = new B();
        asd.get().me();
    }
}
