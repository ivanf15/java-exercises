package com.java8;

import java.util.Optional;

public class OptionalClazz {
    public static void main(String[] args) {
        // First Example
        String[] str = new String[10];
        while(true){
            try {
                // This code could throw an NullPointerException
                String lowercaseString = str[5].toLowerCase();
            }
            catch (NullPointerException ex){
                System.out.println("That is true, it falls into: " + ex);
            }
            finally {
                Optional<String> checkNull = Optional.ofNullable(str[5]);
                if (checkNull.isPresent()){
                    String lowerCaseString = str[5].toLowerCase();
                    System.out.println(lowerCaseString);
                    break;
                }
                else {
                    System.out.println("String value is no present");
                }
            }
            // IfValue is present
            str[5] = "JAVA IS A POWERFUL PROGRAMMING LANGUAGE";
        }
        // Methods examples
        // It returns an empty instance of Optional class
        Optional<String> empty = Optional.empty();
        System.out.println(empty);
        // It returns a non-empty Optional
        Optional<String> value = Optional.of(str[5]);
        value.ifPresent(System.out::println); // Note Consumer as Method Reference
        // If value is present, it returns an Optional otherwise returns an empty Optional
        System.out.println("Filtered value: " + value.filter((s)->s.equals("Abc")));
        System.out.println("Filtered value: " + value.filter((s)->s.equals("JAVA IS A POWERFUL PROGRAMMING LANGUAGE")));
        // It returns value of an Optional. if value is not present, it throws an NoSuchElementException
        System.out.println("Getting value: " + value.get());
        // It returns hashCode of the value
        System.out.println("Getting hashCode: "+value.hashCode());
        // It returns true if value is present, otherwise false
        System.out.println("Is value present: " + value.isPresent());
        // It returns non-empty Optional if value is present, otherwise returns an empty Optional
        System.out.println("Nullable Optional: " + Optional.ofNullable(str[5]));
        // It returns value if available, otherwise returns specified value,
        System.out.println("orElse: " + value.orElse("Value is not present"));
        System.out.println("orElse: " + empty.orElse("Value is not present"));
        value.ifPresent(System.out::println);   // printing value by using method reference
    }
}
