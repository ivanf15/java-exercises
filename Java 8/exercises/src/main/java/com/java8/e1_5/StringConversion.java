package com.java8.e1_5;

public class StringConversion{
    public static void main(String [] args) {
        Double pi;
        String strval; 

        pi = Double.valueOf("3.14");
        strval = String.valueOf(pi);
        System.out.println(strval);
        System.out.println(pi);
    }
}