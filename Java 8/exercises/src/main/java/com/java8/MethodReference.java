package com.java8;

import java.util.function.BiFunction;

interface Sayable {
    void say();
}

interface Messageable{
    Message getMessage(String message);
}

class Message{
    Message(String message){
        System.out.println(message);
    }
}

class Arithmetic{
    public static int add(int a, int b) {
        return a+b;
    }
    public static float add(int a, float b) {
        return a+b;
    }
    public static float add(float a, float b) {
        return a+b;
    }
}
class Arithmetic2{
    public int add(int a,int b){
        return a+b;
    }
}
/*
********************************
* REFERENCE TO A STATIC METHOD *
********************************
*/
public class MethodReference {

    public void printSomething(){
        System.out.println("Printing something");
    }

    public void saySomethingNoStatic(){
        System.out.println("Saying something from no static method");
    }

    public static void saySomething(){
        System.out.println("Saying something....");
    }

    public static void ThreadStatus(){
        System.out.println("Thread is running");
    }

    public static void main(String[] args) {
        // ***********************
        // Referring static method
        // ***********************
        Sayable sayable = MethodReference::saySomething;
        // Calling interface Method
        sayable.say();

        // Using interface Runnable
        Thread t2 = new Thread(MethodReference::ThreadStatus);
        t2.start();

        //You can also use predefined functional interface to refer methods.
        // In the following example, we are using BiFunction interface and using it's apply() method.
        BiFunction<Integer,Integer,Integer> adder = Arithmetic::add;
        int result = adder.apply(12,23);
        System.out.println("Using Bifunctional interface " + result);

        //You can also override static methods by referring methods.
        // In the following example, we have defined and overloaded three add methods.
        BiFunction<Integer,Integer,Integer> adder1 = Arithmetic::add;
        BiFunction<Float,Integer,Float> adder2 = Arithmetic::add;
        BiFunction<Float,Float,Float> adder3 = Arithmetic::add;
        int result1 = adder1.apply(10,22);
        float result2 = adder2.apply(12.6f,10);
        float result3 = adder3.apply(13.5f,193.2f);

        // *********************************
        // Referring to an instance Method *
        // *********************************

        // In the following example, we are referring non-static methods.
        // You can refer methods by class object and anonymous object.
        MethodReference methodReference = new MethodReference(); // By creating an object
        Sayable sayable2 = methodReference::saySomethingNoStatic;
        sayable2.say();
        Sayable sayable3 = new MethodReference()::saySomethingNoStatic;// By using an anonymous object
        sayable3.say();

        // In the following example, we are referring instance (non-static) method.
        // Runnable interface contains only one abstract method. So, we can use it as functional interface.
        Thread t3 = new Thread(new MethodReference()::printSomething);
        t3.start();

        // In the following example, we are using BiFunction interface.
        // It is a predefined interface and contains a functional method apply().
        // Here, we are referring add method to apply method.
        BiFunction<Integer,Integer,Integer> adderN = new Arithmetic2()::add;
        System.out.println(adderN.apply(2,4));

        // ****************************
        // Referring to a Constructor *
        // ****************************

        // You can refer a constructor by using the new keyword.
        // Here, we are referring constructor with the help of functional interface.
        Messageable hello = Message::new;
        hello.getMessage("Waving from method reference");
    }
}
