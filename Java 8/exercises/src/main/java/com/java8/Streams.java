package com.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Streams {
    public static void main(String[] args) {
        List<Product> productsList = new ArrayList<Product>();
        //Adding Products
        productsList.add(new Product(1,"HP Laptop",25000f));
        productsList.add(new Product(2,"Dell Laptop",30000f));
        productsList.add(new Product(3,"Lenevo Laptop",28000f));
        productsList.add(new Product(4,"Sony Laptop",28000f));
        productsList.add(new Product(5,"Apple Laptop",90000f));

        List<Float> productPriceListNoStream = new ArrayList<Float>();

        for(Product p : productsList){
            if(p.price < 30000) productPriceListNoStream.add(p.price);
        }

        List<Float> productPriceListStream = productsList.stream()
                .filter(product -> product.price < 30000)
                .map(product -> product.price)
                .collect(Collectors.toList());
        System.out.println("Without Stream " + productPriceListNoStream);
        System.out.println("With Stream " + productPriceListStream);

        productsList.stream()
                .filter(product -> product.price >= 30000)
                .forEach(Product::printName);

        // This is more compact approach for filtering data
        Float totalPrice = productsList.stream()
                .map(product->product.price)
                .reduce(0.0f,(sum, price)->sum+price);   // accumulating price
        System.out.println(totalPrice);
        // More precise code
        float totalPrice2 = productsList.stream()
                .map(product->product.price)
                .reduce(0.0f,Float::sum);   // accumulating price, by referring method of Float class
        System.out.println(totalPrice2);

        // Filtering
        System.out.println("Filtering...");
        Product productA = productsList.stream()
                .max((product1, product2)->
                        product1.price > product2.price ? 1: -1).get();
        System.out.println(productA.name + productA.price);

        System.out.println("By Method reference applying to mapping");
        Map<Integer,String> productPriceMap =
                productsList.stream()
                        .collect(Collectors.toMap(Product::getId, Product::getName));

        System.out.println(productPriceMap);
    }
}
