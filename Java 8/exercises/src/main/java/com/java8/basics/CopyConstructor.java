package com.java8.basics;

public class CopyConstructor {

        int id;
        String name;

        //constructor to initialize integer and string
        CopyConstructor(int id, String name) {
            this.id = id;
            this.name = name;
        }

        //constructor to initialize another object
        CopyConstructor(CopyConstructor s) {
            id = s.id;
            name = s.name;
        }

        void display() {
            System.out.println(id + " " + name);
        }

    public static void main(String args[]) {
        CopyConstructor s1 = new CopyConstructor(111, "Karan");
        CopyConstructor s2 = new CopyConstructor(s1);
        s1.display();
        s2.display();
        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
        System.out.println(s1.equals(s2));
    }
}
