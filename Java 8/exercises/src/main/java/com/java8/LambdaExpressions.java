package com.java8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Product {
    int id;
    String name;
    public float price;
    public Product(int id, String name, float price){
        this.id= id;
        this.name=name;
        this.price=price;
    }

    public static void printName(Product p){
        System.out.println(p.name);
    }

    public String getName(){
        return name;
    }
    public int getId(){
        return id;
    }
}

public class LambdaExpressions {
    public static void main(String[] args) {
        List<Product> list = new ArrayList<>();

        list.add(new Product(1,"HP Laptop",190.2f));
        list.add(new Product(1,"Dell Laptop",122.2f));
        list.add(new Product(1,"ASUS Laptop",126.2f));

        System.out.println("Sorting on the basis of name...");

        Collections.sort(list,(p1,p2)->(p1.name.compareTo(p2.name)));

        list.forEach(product -> System.out.println("id: " + product.id + " name: " + product.name + " price: " + product.price));
    }
}
