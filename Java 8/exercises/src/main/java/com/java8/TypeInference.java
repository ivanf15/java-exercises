package com.java8;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

class GenericClass<X> {
    X name;
    String msg;
    public void setName(X name){
        this.name = name;
    }
    public X getName(){
        return name;
    }
    public void setMsg(String msg){
        this.msg=msg;
    }
    public String getMsg(){
        return this.msg;
    }
    public String genericMethod(GenericClass<X> x){
        x.setMsg("This class belongs to: " + x);
        return x.getMsg();
    }

    public String genericMethodIn(GenericClass<X> x){
        return x.toString();
    }
}

public class TypeInference {
    public static void showList(List<Integer> list) {
        if(!list.isEmpty()){
            list.forEach(System.out::println);
        }
        else System.out.println("List is empty");
    }

    public static void main(String[] args) {
        // An old approach(prior to Java 7) to create a list
        List<Integer> list1 = new ArrayList<Integer>();
        list1.add(11);
        showList(list1);
        // Java 7
        List<Integer> list2 = new ArrayList<>(); // You can left it blank, compiler can infer type
        list2.add(12);
        showList(list2);
        // Compiler infers type of ArrayList, in Java 8
        showList(new ArrayList<>());

        // Generating a generic class
        GenericClass<String> genericClass = new GenericClass<String>();
        System.out.println("Prior to set name: " + genericClass.getName());
        genericClass.setName("Peter");
        System.out.println(genericClass.getName());


        GenericClass<Integer> genericClass2 = new GenericClass<>();
        genericClass2.setName(32);
        System.out.println(genericClass2.getName());

        // New improved type inference
        System.out.println(genericClass2.genericMethod(new GenericClass<>()));
    }
}
