package com.java8;

import java.util.Base64;

public class BasicEncoding {
    public static void main(String[] args) {
        // Basic encoding in baase 64
        // Getting encoder
        Base64.Encoder encoder = Base64.getEncoder();
        // Creating Byte array
        byte b[] = {2,4};
        // Encoding array
        byte b2[] = encoder.encode(b);
        System.out.println("Encoded byte array: " + b2);

        byte b3[] = new byte[5];                // Make sure it has enough size to store copied bytes
        int x = encoder.encode(b,b3);    // Returns number of bytes written
        System.out.println("Encoded byte array written to another array: "+ b3);
        System.out.println("Number of bytes written: "+ x);

        // Encoding string
        String str = encoder.encodeToString("JavaTpoint".getBytes());
        System.out.println("Encoded string: " + str);
        // Getting decoder
        Base64.Decoder decoder = Base64.getDecoder();
        // Decoding string
        String dStr = new String(decoder.decode(str));
        System.out.println("Decoded string: " + dStr);

        // Encoding ULR
        // Getting encoder
        Base64.Encoder urlEncoder = Base64.getUrlEncoder();
        // Encoding URL
        String eStr = urlEncoder.encodeToString("http://www.javatpoint.com/java-tutorial/".getBytes());
        System.out.println("Encoded URL: " + eStr);
        // Getting decoder
        Base64.Decoder urlDecoder = Base64.getUrlDecoder();
        // Decoding URl
        dStr = new String(urlDecoder.decode(eStr));
        System.out.println("Decoded URL: " + dStr);

        // MIME encoding
        // Getting MIME encoder
        Base64.Encoder mimeEncoder = Base64.getMimeEncoder();
        String message = "Hello, \nYou are informed regarding your inconsistency of work";
        String mimeStr = mimeEncoder.encodeToString(message.getBytes());
        System.out.println("Encoded MIME message: " + mimeStr);

        // Getting MIME decoder
        Base64.Decoder mimeDecoder = Base64.getMimeDecoder();
        // Decoding MIME encoded message
        String mimeDStr = new String(mimeDecoder.decode(mimeStr));
        System.out.println("Decoded message: " + mimeDStr);
    }
}