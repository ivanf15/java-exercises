package com.java8;

class Clonning implements Cloneable {
    int rollno;
    String name;

    Clonning(int rollno, String name) {
        this.rollno = rollno;
        this.name = name;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public static void main(String[] args) {
        try {
            Clonning s1 = new Clonning(101, "amit");

            Clonning s2 = (Clonning) s1.clone();

            System.out.println(s1.rollno + " " + s1.name);
            System.out.println(s2.rollno + " " + s2.name);
            System.out.println(s1.hashCode());
            System.out.println(s2.hashCode());

        } catch (CloneNotSupportedException c) {
        }
    }
}
