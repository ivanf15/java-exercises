package com.java8;

interface Flyable{
    default void fly(){
        System.out.println("Flying from default method");
    }
    abstract void flyInCircles(String message);

    static void flyAround(String mes){
        System.out.println("Flying near from objective: " + mes);
    }
}

public class DefaultMethods implements Flyable {

    @Override
    public void flyInCircles(String message) {
        System.out.println("Flying in circles and message is: " + message);
    }

    public static void main(String[] args) {
        DefaultMethods dm = new DefaultMethods();
        dm.flyInCircles("This is the message send it through method");
        dm.fly();
        Flyable.flyAround("Helicopter");
    }
}
