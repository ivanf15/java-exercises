package com.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import static java.util.Arrays.asList;

public class Stringjoiner {
    public static void main(String[] args) {
        List<String> names  = asList("Rahul","Raju","Peter","Raheem");

        // String Joiner
        StringJoiner joinNames = new StringJoiner(","); // passing comma(,) as delimiter

        // Adding prefix and suffix
        StringJoiner joinerPreSuf = new StringJoiner(";","[","]");

        // Adding to each StringJoiner Object
        names.forEach(joinNames::add);
        names.forEach(joinerPreSuf::add);

        // Merge two Strings
        StringJoiner merge = joinerPreSuf.merge(joinNames);

        System.out.println(joinNames);
        System.out.println(joinerPreSuf);
        System.out.println("Merge: " + merge);
    }
}
