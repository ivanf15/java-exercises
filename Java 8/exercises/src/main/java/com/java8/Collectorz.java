package com.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Collectorz {
    // Fetching data into a list
    public static void main(String[] args) {
        List<Product> productsList = new ArrayList<Product>();
        //Adding Products
        productsList.add(new Product(1,"HP Laptop",25000f));
        productsList.add(new Product(2,"Dell Laptop",30000f));
        productsList.add(new Product(3,"Lenevo Laptop",28000f));
        productsList.add(new Product(4,"Sony Laptop",28000f));
        productsList.add(new Product(5,"Apple Laptop",90000f));
        // Using List
        List<Float> productPriceList = productsList.stream()
                        .map(x->x.price)         // fetching price
                        .collect(Collectors.toList());  // collecting as list
        // Using Set
        Set<Float> productPriceSet  = productsList.stream().map(x->x.price) .collect(Collectors.toSet());
        // Using Sum method
        Double sumPrices = productsList.stream()
                        .collect(Collectors.summingDouble(x->x.price));  // collecting as list
        Integer sumId = productsList.stream().collect(Collectors.summingInt(x->x.id));
        // Getting average
        Double average = productsList.stream()
                .collect(Collectors.averagingDouble(p->p.price));
        // Counting
        Long noOfElements = productsList.stream()
                .collect(Collectors.counting());

        System.out.println(productPriceList);
        System.out.println(productPriceSet);
        System.out.println("Sum of prices: " + sumPrices);
        System.out.println("Sum of id's: " + sumId);
        System.out.println("Average price is: "+average);
        System.out.println(noOfElements);
    }

}
