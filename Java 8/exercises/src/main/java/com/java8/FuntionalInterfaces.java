package com.java8;

// An Interface that have only one method is called in that way.

// Example1
@FunctionalInterface
interface Moveable{
    void move(int x,int y);
}

// Example 1
class FunctionalInterfaces implements Moveable{

    @Override
    public void move(int x, int y) {
        System.out.println("moving to : (" + x + "," + y + ")");
    }

    public static void main(String[] args) {
        FunctionalInterfaces fi = new FunctionalInterfaces();
        fi.move(2,4);
    }
}

// Example 2
// A functional interface can have methods of object class.
// See in the following example.
@FunctionalInterface
interface Speakable{
    void say(String msg);
    // It contain any number od Object class methods
    int hashCode();
    String toString();
    boolean equals(Object obj);
}

class Example2 implements Speakable{
    @Override
    public void say(String msg) {
        System.out.println(msg);
    }

    public static void main(String[] args) {
        Example2 f2 = new Example2();
        f2.say("Saying from example 2");
    }
}

// Example 3
// In the following example, a functional interface is
// extending to a non-functional interface.

interface Doable{
    default void doIt(){
        System.out.println("Do it now!");
    }
}
@FunctionalInterface
interface Breakable extends Doable{
    void breaking(String msg); // Abstract method
}

class Example3 implements Breakable{
    @Override
    public void breaking(String msg) {
        System.out.println(msg);
    }

    public static void main(String[] args) {
        Example3 e3 = new Example3();
        e3.breaking("Coming from child");
        e3.doIt();
    }
}