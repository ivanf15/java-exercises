package com.java8;

class Animal {
    void eat() {
        System.out.println("eating...");
    }
}

// H1 and H2 are hierarchical inheritance

// Single inheritance
// H1
class Dog extends Animal {
    void bark() {
        System.out.println("barking...");
    }
}
// H2
class Cat extends Animal{
    void meow(){
        System.out.println("Meowing...");
    }
}

// Multilevel Inheritance
class PuppyDog extends Dog {
    void weep() {
        System.out.println("Weeping");
    }
}

public class SingleInheritance {
    public static void main(String... args) throws CloneNotSupportedException {
        Dog d = new Dog();
        d.bark();   // Method of Dod Class
        d.eat();    // Method of Animal class

        Cat c = new Cat();
        c.meow();  // Method of Cat class
        c.eat();   // Method of Animal class

        PuppyDog pd= new PuppyDog();
        pd.weep();  // Method of PuppyDog class
        pd.bark();  // Method of Dog Class
        pd.eat();   // Method of Animal Class
    }
}
