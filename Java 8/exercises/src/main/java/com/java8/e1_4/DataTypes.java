package com.java8.e1_4;

public class DataTypes {

    public static void main(String[] args) {
        int integer = 1010;
        boolean bool = true;
        char character = 'a';
        float floating = 10.123456f;
        byte bite = 127;
        long longs = 24354365;
        double doubles = 29.128763;
        short shorts = 3276;
    }
}